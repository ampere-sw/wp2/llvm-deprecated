## Compile LLVM:
Contains the LLVM compiler sources, to compile extract the sources and run:

```sh
$ mkdir llvm-build
$ cd llvm-build
$ ../llvm/setup-cmake.sh 
```
If you have ninja-build installed in your system run:

```sh
$ ninja && ninja install
```

if you dont, just run make:

```sh                              
$ make -j8 && make install
```
The default install directory will be llvm-install. Add llvm-install/bin to your PATH environment variable.

## Run LLVM analysis on the example:

Use the "run_auto.sh" scritp, replacing the paths for LLVM and the sources.
