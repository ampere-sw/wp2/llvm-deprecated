#Compile TicksUtils
/fio/home/amunera/llvm-updated/llvm-install/bin/clang -emit-llvm -S -fompss-2  -Xclang -disable-llvm-passes -Xclang -disable-O0-optnone -Isynthetic_gen/ticksUtils/_inc synthetic_gen/ticksUtils/_src/ticksUtils.c
#Compile Labels
/fio/home/amunera/llvm-updated/llvm-install/bin/clang -emit-llvm -S -fompss-2  -Xclang -disable-llvm-passes -Xclang -disable-O0-optnone -Isynthetic_gen/labels/_inc  synthetic_gen/labels/_src/labels.c
#Compile Runnables
/fio/home/amunera/llvm-updated/llvm-install/bin/clang -emit-llvm -S -fompss-2  -Xclang -disable-llvm-passes -Xclang -disable-O0-optnone -Isynthetic_gen/labels/_inc -Isynthetic_gen/ticksUtils/_inc  -Isynthetic_gen/runnables/_inc   synthetic_gen/runnables/_src/runnables.c
#Compile Tasks
/fio/home/amunera/llvm-updated/llvm-install/bin/clang -emit-llvm -S -fompss-2  -Xclang -disable-llvm-passes -Xclang -disable-O0-optnone -Isynthetic_gen/labels/_inc -Isynthetic_gen/ticksUtils/_inc  -Isynthetic_gen/tasks/_inc -Isynthetic_gen/runnables/_inc  synthetic_gen/tasks/_src/tasks.c
#Compile Main
/fio/home/amunera/llvm-updated/llvm-install/bin/clang -emit-llvm -S -fompss-2  -Xclang -disable-llvm-passes -Xclang -disable-O0-optnone -Isynthetic_gen/labels/_inc -Isynthetic_gen/ticksUtils/_inc  -Isynthetic_gen/tasks/_inc -Isynthetic_gen/runnables/_inc -Isynthetic_gen  Executable/main/_src/main.c
#Merge all IR files
/fio/home/amunera/llvm-updated/llvm-install/bin/llvm-link -S labels.ll ticksUtils.ll runnables.ll tasks.ll main.ll -o synthetic.ll

#Run autoscoping & autodeps
/fio/home/amunera/llvm-updated/llvm-install/bin/opt -mem2reg -loop-rotate -ompss-2 -S -print-verbosity=autoscope -disable-autorelease <synthetic.ll> /dev/null
